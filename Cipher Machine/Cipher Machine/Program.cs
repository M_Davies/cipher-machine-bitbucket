﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cipher_Machine
{
    class Program
    {
        static void Main(string[] args)
        {            
            //Run.
            NewMessage();
        }

        public static char[] Alphabet()
        {
            //Create the alphabet.
            char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            
            return alphabet;
        }

        public static int Key()
        {
            //User inputs key.
            Console.WriteLine("Please enter your key");
            int key = Convert.ToInt32(Console.ReadLine());
            return key;
        }       

        public static char EncipherMessage(char ch, int key)
        {
            //Space/Int char validation. Assigns random unicode charecter.
            if (!char.IsLetter(ch))
            {
                Console.WriteLine("ERROR. INVALID CHARECTER DETECTED (Please no spaces)");
                NewMessage();
            }

            //Same as if statement. Checks which case char is.
            char caseCheck = char.IsUpper(ch) ? 'A' : 'a';

            //Adds char charecter identifiers together, then checks its a legit part of alphabet. 
            //Uses char cast to ignore syntax errors. Returns enciphered char.
            return (char) ((((ch + key) - caseCheck) % 26) + caseCheck); 
        }

        public static char DecipherMessage(char ch, int key)
        {
            //Decipher and return. Resets charecter to original position in alphabet.
            return EncipherMessage(ch, 26 - key);

        }

        private static void NewMessage()
        {
            //Intro and option.
            Console.WriteLine("********************************************");
            Console.WriteLine("WELCOME TO THE CAESAR CIPHER!");
            Console.WriteLine("Would you like to:\n1)Cipher a Message\n2)Decipher a message");
            Console.WriteLine("********************************************");
            int response = Convert.ToInt32(Console.ReadLine());
            //Uses numbers to use direction of cipher. Also includes error handling.
            if (response==1)
            {
                Cipher();
            }
            else if (response==2)
            {
                Decipher();
            }
            else
            {
                Console.WriteLine("ERROR - PLEASE ENTER 1 OR 2 TO CHOOSE THE DIRECTION OF CIPHER");
            }
        }

        private static void Cipher()
        {
            //Enter Key and import alphabet.
            char[] alphabet = Alphabet();
            int key = Key();
            
            //Enter plain text message
            Console.WriteLine("Enter message to encrypt (No spaces)");
            string message = Console.ReadLine();
            
            //Convert input to char array.
            char[] messageArray = message.ToCharArray();

            //Encipher.
            string output = string.Empty;
            foreach (char charecter in messageArray)
            {
                output += EncipherMessage(charecter, key);
            }

            //Print out enciphered message and clear console.
            Console.Clear();
            Console.WriteLine("Your enciphered message is " + output);

            //Again?
            Exit();           
        }

        private static void Decipher()
        {
            //Enter key and import alphabet
            char[] alphabet = Alphabet();
            int key = Key();

            //User enters enciphered message.
            Console.WriteLine("Enter enciphered message");
            string message = Console.ReadLine();

            //Fill array and decipher.
            char[] messageArray = message.ToCharArray();

            string output = string.Empty;
            foreach (char charecter in messageArray)
            {
                output += DecipherMessage(charecter, key);
            }

            //Print out deciphered message and clear console.
            Console.Clear();
            Console.WriteLine("Your deciphered message is: " + output);

            //Again?
            Exit();
        }

        public static void Exit()
        {
            //Exit program?
            Console.WriteLine("Close Program? Y/N");
            if (Console.ReadLine().ToUpper() == "Y")
            {
                //Exit.
                Environment.Exit(0);
            }
            else
            {
                //Clear and restart.
                Console.Clear();
                NewMessage();
            }
        }
    }
}
